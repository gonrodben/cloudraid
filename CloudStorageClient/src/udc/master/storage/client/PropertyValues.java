package udc.master.storage.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyValues {
	
	public static String URL;
	public static String USER;
	public static String PASSWORD;
	public static String FOLDER;
	
	private static PropertyValues instance;
	
	private PropertyValues()
	{
		getPropValues();
	}
	
	public static PropertyValues getInstance()
	{
		if(instance == null)
			instance = new PropertyValues();
		
		return instance;
	}
	
	private void getPropValues() {
	    Properties prop = new Properties();
	    String propFileName = "config.properties";
	
	    /*InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	    
	    try {
			prop.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    if (inputStream != null) {
	       
		    result[0] = prop.getProperty("URL");
		    result[1] = prop.getProperty("USER");
		    result[2] = prop.getProperty("PASSWORD");
	    }*/
	    
	    URL = "http://localhost:9992/ws/image?wsdl";
	    USER = "gonzalo";
	    PASSWORD = "123456";
	    FOLDER = "/home/gonzalo/CloudTest/Files";
	}
}
