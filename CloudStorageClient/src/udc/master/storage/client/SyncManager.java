package udc.master.storage.client;

import java.io.File;

import org.apache.commons.vfs.FileSystemException;

public class SyncManager 
{
	StorageClient client;
	
	public SyncManager()
	{
		client = new StorageClient();
	}
	
	public void uploadFolder()
	{
		upload(PropertyValues.getInstance().FOLDER);
		System.out.println( "Upload Folder Done!");
	}
	
	private void upload( String path ) {

        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
            	upload( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile().getPath() );
                client.addFolder(f.getAbsoluteFile().getPath() + "/");
            }
            else {
                System.out.println( "File:" + f.getAbsoluteFile() );
                client.addFile(f.getAbsoluteFile().getPath());
            }
        }
    }
	
	public void downloadFolder(String path)
	{
		String [] objects = client.listObjects();
		
		for(int i = 0; i < objects.length; i++)
		{
			if(objects[i].equals("|"))
				continue;
			
			String absolutePath = path + "/" + objects[i];
			File file = new File(absolutePath);
			File folder = new File(absolutePath);
			
			if(!absolutePath.endsWith("/"))
			{
				if(file.getParent() != "")
				{
					folder = new File(file.getParent());
					folder.mkdirs();
				}
				
				client.getFile(objects[i], absolutePath);
			}
			else
				folder.mkdirs();
		}
	}
	
	public void startSync()
	{
		FileMonitoring fm = new FileMonitoring();
		try {
			fm.startListening(fm);
		} catch (FileSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
