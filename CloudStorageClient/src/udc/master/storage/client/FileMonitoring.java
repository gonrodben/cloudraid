package udc.master.storage.client;

import org.apache.commons.vfs.FileChangeEvent;
import org.apache.commons.vfs.FileListener;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.FileSystemManager;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileMonitor;
 
public class FileMonitoring implements FileListener {
 
private static int counter = 0; //for versioning
private static DefaultFileMonitor fileMonitor = null;

StorageClient client = new StorageClient();


 
public void startListening(FileMonitoring fileMonitoring) throws FileSystemException, InterruptedException 
{
	FileSystemManager fsManager = VFS.getManager();
	FileObject fileObject = fsManager.resolveFile(PropertyValues.getInstance().FOLDER);
	
	if (fileMonitor == null) {
		fileMonitor = new DefaultFileMonitor(this);
		fileMonitor.addFile(fileObject);
		fileMonitor.start();
	}
	
	synchronized (fileMonitoring) {
		fileMonitoring.wait(); //wait fileChange
		startListening(fileMonitoring); //recursive call
	}
}
 
public void fileCreated(FileChangeEvent fce) throws Exception {
	System.out.println("fileCreated2 --> " + fce.getFile().getName());
	
	client.addFile(fce.getFile().getName().getPath());
	
	synchronized (this) { //notify waiting thread
	this.notifyAll();
	}
}
 
public void fileDeleted(FileChangeEvent fce) throws Exception {
	System.out.println("fileDeleted2 --> " + fce.getFile().getName());
	
	client.deleteFile(fce.getFile().getName().getPath());
	
	synchronized (this) 
	{ //notify waiting thread
		this.notifyAll();
	}
}
 
public void fileChanged(FileChangeEvent fce) throws Exception {
	System.out.println("fileChanged2 --> " + fce.getFile().getName());
	
	client.updateFile(fce.getFile().getName().getPath());
	synchronized (this) 
	{ //notify waiting thread
		this.notifyAll();
	}
}

}