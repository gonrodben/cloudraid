package udc.master.storage.client;

import java.awt.Image;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOMFeature;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.commons.vfs.FileSystemException;

import udc.master.storage.service.WebServiceInterface;

public class Main {

	public static void main(String[] args) {
		
		SyncManager sync = new SyncManager();
		//sync.uploadFolder();
		//sync.startSync();
		sync.downloadFolder("/home/gonzalo/CloudTest/Files2");
		
		//StorageClient client = new StorageClient();
		//client.addFile("b", "/home/gonzalo/CloudTest/Files/b.pdf");
		//client.getFile("a", "/home/gonzalo/CloudTest/Files2/a");
		System.out.print("Ok");
	}
	
	

}
