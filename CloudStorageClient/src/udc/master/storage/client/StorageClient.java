package udc.master.storage.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;

import udc.master.storage.service.WebServiceInterface;

public class StorageClient 
{
	String user;
	String password;
	String ws_url;
	String bucket = "Bucket";
	WebServiceInterface server;
	
	public StorageClient()
	{
		ws_url = PropertyValues.getInstance().URL;
        user = PropertyValues.getInstance().USER;
        password = PropertyValues.getInstance().PASSWORD;
        
		URL url = null;
		try {
			url = new URL(ws_url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        QName qname = new QName("http://service.storage.master.udc/", "WebServiceImplService");
 
        Service service = Service.create(url, qname);
        server = service.getPort(WebServiceInterface.class);

        verify();
	}
	
	private void addHeaders()
	{
		 Map<String, Object> req_ctx = ((BindingProvider)server).getRequestContext();
	     req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, ws_url);
	     
	     Map<String, List<String>> headers = new HashMap<String, List<String>>();
	     headers.put("Username", Collections.singletonList(user));
	     headers.put("Password", Collections.singletonList(password));
	     req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
	}
	
	public void verify()
	{
		boolean created = server.createUser(user, password, "Usuario", "correo@gmail.com");
		
		if(created)
		{
			addHeaders();
			server.createBucket(bucket);
		}
	}

	public void addFolder(String path)
	{
		String name = path.replaceFirst(PropertyValues.getInstance().FOLDER + "/", "");
		addHeaders();
		server.createFolder(bucket, name);
	}
	
	public void addFile(String path)
	{
		String name = path.replaceFirst(PropertyValues.getInstance().FOLDER + "/", "");
		addFile(name, path);
	}

	public void addFile(String name, String path)
	{
		byte[] data = new byte[0];
		File file = new File(path);
		
		try 
		{
			FileInputStream fis = new FileInputStream(file);
			data = new byte[(int)file.length()];
			fis.read(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		addHeaders();
		server.uploadFile(bucket, name, data);
	}
	
	public void updateFile( String path)
	{
		String name = path.replaceFirst(PropertyValues.getInstance().FOLDER + "/", "");
		addFile(name, path);
	}

	public void updateFile(String name, String path)
	{
		byte[] data = new byte[0];
		File file = new File(path);
		
		try 
		{
			FileInputStream fis = new FileInputStream(file);
			data = new byte[(int)file.length()];
			fis.read(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		addHeaders();
		server.updateFile(bucket, name, data);
	}
	
	
	public void getFile(String name, String path)
	{
		System.out.print("name:" + name);
		System.out.print("path:" + path);
		
		addHeaders();
		byte [] data = server.downloadFile(bucket, name);
		
		try {	
			File file = new File(path);
			file.createNewFile();
			
			FileOutputStream fos = new FileOutputStream(file);
			
			fos.write(data);
			fos.flush();
			fos.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void deleteFile(String path)
	{
		String name = path.replaceFirst(PropertyValues.getInstance().FOLDER + "//", "");
		addHeaders();
		server.deleteFile(bucket, name);
	}
	
	public String[] listObjects()
	{
		addHeaders();
		String objects = server.listBucketObjectsPath(bucket);
		
		String[] partes = objects.split("\\|");
		
		//for(int i = 0; i < partes.length; i++)
		//	System.out.print(partes[i] + "      ");
		
		return partes;
	}
}
