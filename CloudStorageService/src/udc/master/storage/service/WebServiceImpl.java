package udc.master.storage.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import udc.master.storage.BucketAccess;
import udc.master.storage.DataStorageException;
import udc.master.storage.FileSystem;

//import org.apache.axiom.attachments.ByteArrayDataSource;

//Service Implementation Bean
@MTOM
@WebService(endpointInterface = "udc.master.storage.service.WebServiceInterface")
public class WebServiceImpl implements WebServiceInterface{
 
	@Resource
    WebServiceContext wsctx;
	
	@Override 
	public boolean createUser(String username, String password, String name, String email)
	{
		FileSystem fs = new FileSystem();
		try {
			fs.createUser(username, password, name, email);
		} catch (DataStorageException e) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public boolean shareBucket(String username, String bucket, boolean allowWrite, boolean allowShare)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try {
			BucketAccess access = allowShare ? BucketAccess.ADMIN : allowWrite ? BucketAccess.WRITE : BucketAccess.READ;
			fs.shareBucket(bucket, username, access);
		} catch (DataStorageException e) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public boolean unshareBucket(String username, String bucket)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		fs.unshareBucket(username, bucket);

		return true;
	}
	
	@Override 
	public String listBuckets()
	{
		String result = "";
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		List<String> buckets = fs.listUserBuckets(user);
		
		for(int i = 0; i < buckets.size(); i++)
			result += ((result != "" ? "|" : "") + buckets.get(i));
		
		return result;
	}
	
	@Override 
	public boolean createBucket(String bucket)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try {
			fs.createBucket(bucket, user);
		} catch (DataStorageException e) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public String listBucketObjectsPath(String bucket)
	{
		String result = "";
		
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		List<String> objects = fs.listBucketObjectsPath(bucket, user);
		
		for(int i = 0; i < objects.size(); i++)
			result += ((result != "" ? "|" : "") + objects.get(i));
		
		return result;
	}
	
	@Override 
	public DataHandler downloadFile(String bucket, String filename)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try 
		{
			byte[] byteArray = fs.getFileData(bucket, user, filename);
			return toDataHandler(byteArray);
		} 
		catch (DataStorageException | IOException e) {
			throw new WebServiceException("Download Failed!");
		}
	}
 
	@Override 
	public boolean uploadFile(String bucket, String filename, DataHandler data)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try {
			byte[] byteArray;
			byteArray = toBytes(data);
			fs.addFile(bucket, user, filename, byteArray);
		} catch (IOException | DataStorageException e) {
			return false;
		}
		
		return true;
	}

	@Override 
	public boolean updateFile(String bucket, String filename, DataHandler data)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try {
			byte[] byteArray = toBytes(data);
			fs.addFile(bucket, user, filename, byteArray);
		} catch (IOException | DataStorageException e) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public boolean deleteFile(String bucket, String filename)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		try {
			fs.deleteFile(bucket, user, filename);
		} catch (DataStorageException e) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public boolean createFolder(String bucket, String foldername)
	{
		FileSystem fs = new FileSystem();
		String user = verifyUser(fs);
		
		if(user == "")
			throw new WebServiceException("El usuario no es válido.");
		
		fs.createFolder(bucket, foldername);
		return true;
	}
	
	
	private byte[] toBytes(DataHandler handler) throws IOException {
	    ByteArrayOutputStream output = new ByteArrayOutputStream();
	    handler.writeTo(output);
	    return output.toByteArray();
	}
	
	private DataHandler toDataHandler(byte[] data) throws IOException {
		//ByteArrayDataSource dataSource = new ByteArrayDataSource(data);
		InputStream is = new ByteArrayInputStream(data);
	    return new DataHandler(new InputStreamDataSource(is));
	}
	
	private String verifyUser(FileSystem fs)
	{
			MessageContext mctx = wsctx.getMessageContext();
		 
			//get detail from request headers
	        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
	        List userList = (List) http_headers.get("Username");
	        List passList = (List) http_headers.get("Password");
	 
	        String username = "";
	        String password = "";
	 
	        if(userList!=null)
	        	username = userList.get(0).toString();

	        if(passList!=null)
	        	password = passList.get(0).toString();

	        if(fs.verifyUser(username, password))
	        	return username;
		
	        return "";
	}
	
}