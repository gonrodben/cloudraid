package udc.master.storage.service;

import java.util.List;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface WebServiceInterface {
		
		@WebMethod 
		boolean createUser(String username, String password, String name, String email);
		
		@WebMethod 
		boolean shareBucket(String username, String bucket, boolean allowWrite, boolean allowShare);
		
		@WebMethod
		boolean unshareBucket(String username, String bucket);
		
		@WebMethod
		String listBuckets();
		
		@WebMethod
		boolean createBucket(String bucket);
		
		@WebMethod
		String listBucketObjectsPath(String bucket);
		
		@WebMethod 
		DataHandler downloadFile(String bucket, String filename);
	 
		@WebMethod 
		boolean uploadFile(String bucket, String filename, DataHandler data);

		@WebMethod
		boolean updateFile(String bucket, String filename, DataHandler data);
		
		@WebMethod
		boolean deleteFile(String bucket, String filename);
		
		@WebMethod
		boolean createFolder(String bucket, String foldername);
}
