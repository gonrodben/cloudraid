
package udc.master.storage;

public class DataStorageException extends Exception {
	public DataStorageException(String message)
	{
			super(message);
	}
}
