
package udc.master.storage.raid;

public class RAIDFactory 
{
	public static VirtualRAID getFromConfigString(String config)
	{
		String [] params = config.split(";");
		
		if(params.length == 4 && params[0] == "StrippedRAID")
		{
			return new StrippedRAID(Integer.parseInt(params[1]), Integer.parseInt(params[2]), Integer.parseInt(params[3]));
		}
		
		return null;
	}
}
