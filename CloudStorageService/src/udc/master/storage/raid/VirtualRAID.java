
package udc.master.storage.raid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public abstract class VirtualRAID implements IVirtualHDD
{ 
	List<IVirtualHDD> discArray;
	int blockSize = 0;
	int currentDrive = -1;
	String identifier;
	
	public VirtualRAID(String identifier, int blockSize)
	{
		this(blockSize);
		this.identifier = identifier;
	}
	
	public VirtualRAID(int blockSize)
	{
		discArray = new ArrayList<IVirtualHDD>();
		this.blockSize = blockSize;
	}

	@Override
	public abstract List<BlockInfo> write(byte[] data) throws IOException;
	
	@Override
	public abstract void read(List<BlockInfo> blocks, byte[] buffer, int startIndex) throws IOException;

	@Override
	public abstract boolean delete(List<BlockInfo> blocks) throws IOException;
	
	@Override
	public abstract int getDrivesNumber();
	
	public abstract boolean isValid();
	
	public abstract String getConfiString();
	
	@Override
	public String getIdentifier()
	{
		return identifier;
	}
	
	@Override
	public void setIdentifier(String id)
	{
		identifier = id;
	}

	public int getBlockSize(IVirtualHDD drive)
	{
		return blockSize * drive.getDrivesNumber();
	}
	
	public int getBlockSize()
	{
		return blockSize;
	}
	
	public void setBlockSize(int blockSize)
	{
		this.blockSize = blockSize;
	}
	
	public void addHDD(IVirtualHDD hdd)
	{
		discArray.add(hdd);
	}
	
	public void removeHDD(IVirtualHDD hdd)
	{
		discArray.remove(hdd);
	}
	
	public void removeHDD(String id)
	{
		IVirtualHDD hdd = getHDD(id);
		
		if(hdd != null)
			discArray.remove(hdd);
	}
	
	public IVirtualHDD getHDD(int index)
	{
		return discArray.get(index);
	}
	
	public IVirtualHDD getHDD(String id)
	{
		IVirtualHDD hdd = null;
		
		for(int i = 0; i < discArray.size(); i++)
		{
			if(discArray.get(i).getIdentifier().equals(id))
			{
				hdd = discArray.get(i);
				break;
			}
			
			try
			{
				VirtualRAID raid = (VirtualRAID)discArray.get(i);
				hdd = raid.getHDD(id);
				
				if(hdd != null)
					return discArray.get(i);
			}
			catch(Exception ex)
			{
				
			}
		}
		
		return hdd;
	}
	
	public int size()
	{
		return discArray.size();
	}
	
	public int getCurrentDriveIndex(int index)
	{
		return currentDrive;
	}

	public IVirtualHDD next()
	{
		if(size() == 0)
			return null;
		
		currentDrive++;
		
		if(currentDrive >= size())
			currentDrive = 0;
		
		return getHDD(currentDrive);
	}
}
