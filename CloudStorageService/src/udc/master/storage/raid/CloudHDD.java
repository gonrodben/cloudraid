package udc.master.storage.raid;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CloudHDD implements IVirtualHDD {
	
	CloudProvider provider;
	
	public CloudHDD(CloudProvider provider)
	{
		this.provider = provider;
	}

	@Override
	public String getIdentifier() {
		return provider.getProviderIdentifier();
	}

	@Override
	public void setIdentifier(String id) {
		
	}

	@Override
	public List<BlockInfo> write(byte[] data) throws IOException,
			IOException {
		
		String filename = java.util.UUID.randomUUID().toString();
			
		provider.uploadFile(filename, data);
		
		List<BlockInfo> blocks = new ArrayList<BlockInfo>();
		BlockInfo block = new BlockInfo(getIdentifier(), filename, data.length);
		blocks.add(block);
		
		return blocks;
	}

	@Override
	public void read(List<BlockInfo> blocks, byte[] buffer, int startIndex) throws IOException {
		
		BlockInfo block = blocks.get(0);
		String filename = block.getBlockIdentifier();
		byte [] data = provider.downloadFile(filename);
		RAIDHelper.setSection(buffer, data, startIndex);
		
		/*for(int i = 0; i < blocks.size(); i++)
		{
			BlockInfo block = blocks.get(i);
			String filename = block.getBlockIdentifier();
			byte [] data = provider.downloadFile(filename);
			
			RAIDHelper.setSection(buffer, data, block.index);
		}*/
	}
	
	@Override
	public boolean delete(List<BlockInfo> blocks) throws IOException
	{
		boolean result = true;
		
		/*for(int i = 0; i < blocks.size(); i++)
		{
			BlockInfo block = blocks.get(i);
			String blockPath = dataPath + "/" + block.getBlockIdentifier();
			File file = new File(blockPath);
			if(!file.delete())
				result = false;
		}*/
		
		return result;
	}

	@Override
	public int getDrivesNumber() {
		return 1;
	}

}
