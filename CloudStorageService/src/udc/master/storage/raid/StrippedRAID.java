
package udc.master.storage.raid;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import udc.master.storage.datastrip.Block;
import udc.master.storage.datastrip.DataStripException;
import udc.master.storage.datastrip.DataStripper;
import udc.master.storage.datastrip.JerasureDataStripper;
import udc.master.storage.log.Log;


public class StrippedRAID extends VirtualRAID {

	int k; 
	int m;
	
	public StrippedRAID(int k, int m, int blockSize)
	{
		super(blockSize);
		this.k = k; 
		this.m = m;
	}

	public boolean isValid()
	{
		return size() >= k + m;
	}
	
	public DataStripper getDataStripper()
	{
		return new JerasureDataStripper(k, m, blockSize); 
	}

	public List<BlockInfo> write(byte[] data) throws IOException
	{
		List<BlockInfo> dataBlocks = new ArrayList<BlockInfo>();
		DataStripper stripper = getDataStripper();
		List<Block> blocks = stripper.encode(data);
		
		for(int i = 0; i < blocks.size(); i++)
		{
			Block block = blocks.get(i);
			IVirtualHDD hdd = next();
			List<BlockInfo> result = hdd.write(block.getData());   //HILOOO  //Mandar tantos bloques como discos tenga el HDD  //Que reciba una lista de bloques..
			
			for(int j = 0; j < result.size(); j++)
			{
				BlockInfo dataBlock = result.get(j);
				dataBlock.isParityBlock = block.IsCoding();
				dataBlock.row = block.getRow();
				dataBlock.index = block.getIndex();
				dataBlocks.add(dataBlock);
			}
		}
		
		return dataBlocks;
	}
	
	public void read(List<BlockInfo> blocks, byte[] buffer, int startIndex) throws IOException
	{
		HashMap<Integer, List<BlockInfo>> matrix =  getBlocksMatrix(blocks);
		
		for(int i = 0; i < matrix.size(); i++)
		{
			List<BlockInfo> blocksList = matrix.get(i);
			try 
			{
				readSrip(blocksList, buffer, startIndex + (i*(k)*getBlockSize()));
			} 
			catch (RAIDException e) 
			{
				Log.RecordError("RAIDService", "Error RAID al leer las secciones del fichero." , e);
				throw new IOException("Error RAID al leer las secciones del fichero.");
			}
		}
	}
	
	public void readSrip(List<BlockInfo> dataBlocks, byte[] buffer, int startIndex) throws RAIDException
	{
		List<Block> blocks = new ArrayList<Block>();
		//Se podría hacer que sólo llame al stripper si falla algún disco de datos
		
		for(int i = 0; i < dataBlocks.size(); i++)
		{
			BlockInfo dataBlock = dataBlocks.get(i);
			
			byte[] data = new byte[blockSize];
			if(!read(dataBlock, data, 0))
				data = null;
			
			Block block = new Block(data, dataBlock.isParityBlock, dataBlock.index, dataBlock.row);
			blocks.add(block);	
		}
		
		DataStripper stripper = getDataStripper();
		try 
		{
			stripper.decode(blocks, buffer, startIndex);
		} 
		catch (DataStripException e) 
		{
			Log.RecordError("RAIDService", "Error al decodificar las secciones del fichero" , e);
			throw new RAIDException("Error al decodificar las secciones del fichero");
		}
	}
	
	private boolean read(BlockInfo block, byte[] buffer, int startIndex)
	{
		try 
		{
			IVirtualHDD hdd = getHDD(block.getHDDIdentifier());
			
			if(hdd == null)
			{
				Log.RecordError("RAIDService", "Unidad de almacenamiento no encontrada: " + block.getHDDIdentifier(), "");
				return false;
			}
			
			List<BlockInfo> list = new ArrayList<BlockInfo>();
			list.add(block);
		
			hdd.read(list, buffer, startIndex);
			return true;
			
		} catch (IOException e) {
			Log.RecordError("RAIDService", "Error en la unidad de almacenamiento: " + block.getHDDIdentifier() + ". No se pudo recuperar el bloque: " + block.getBlockIdentifier() , e);
			return false;
		}
	}
	
	@Override
	public boolean delete(List<BlockInfo> blocks) throws IOException
	{
		boolean result = true;
		
		for(int i = 0; i < blocks.size(); i++)
		{
			BlockInfo block = blocks.get(i);
			IVirtualHDD hdd = getHDD(block.getHDDIdentifier());
			List<BlockInfo> blocksToDelete = new ArrayList<BlockInfo>();
			blocksToDelete.add(block);
			if(!hdd.delete(blocksToDelete))
				result = false;
		}
		
		return result;
	}
	
	private HashMap<Integer, List<BlockInfo>> getBlocksMatrix(List<BlockInfo> blocks)
	{
		HashMap<Integer, List<BlockInfo>> matrix = new HashMap<Integer, List<BlockInfo>>();
		
		for(int i = 0; i < blocks.size(); i++)
		{
			List<BlockInfo> list = new ArrayList<BlockInfo>();
			BlockInfo block = blocks.get(i);
			int row = block.row;
			
			if(matrix.containsKey(row))
				list = matrix.get(row);
			else
				matrix.put(row, list);
			
			list.add(block);
		}
		
		return matrix;
	}
	
	public int getDrivesNumber()
	{
		return size() - 1;
	}
	
	public String getConfiString()
	{
		return "StrippedRAID;" + k + ";" + m + ";" + getBlockSize();
	}
}
