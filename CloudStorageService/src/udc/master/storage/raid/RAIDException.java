
package udc.master.storage.raid;

public class RAIDException extends Exception {
	public RAIDException(String message)
	{
		super(message);
	}
	
	public RAIDException()
	{
		super();
	}
}
