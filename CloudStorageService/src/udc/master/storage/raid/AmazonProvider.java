package udc.master.storage.raid;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;


public class AmazonProvider extends CloudProvider {

	AWSCredentials credentials = null;
	AmazonS3 s3 = null;
	
	public AmazonProvider(String accesskey, String secretKey, String rootPath)
	{
		super(accesskey, secretKey, rootPath);

	    credentials = new BasicAWSCredentials(accessKey, secretKey);
	    s3 = new AmazonS3Client(credentials);
	    Region usWest2 = Region.getRegion(Regions.US_WEST_2);
	    s3.setRegion(usWest2);
	}
	
	@Override
	public void uploadFile(String filename, byte[] content) throws IOException {
		
			Boolean bucketExists = false;
			
			 for (Bucket bucket : s3.listBuckets()) {
	                if(rootPath == bucket.getName())
	                {
	                	bucketExists = true;
	                	break;
	                }
	            }
		
		if(!bucketExists)
			s3.createBucket(rootPath);
		
		InputStream input = new ByteArrayInputStream(content);
		ObjectMetadata metadata = new ObjectMetadata();
		
		s3.putObject(new PutObjectRequest(rootPath, filename, input, metadata));
	}

	@Override
	public byte[] downloadFile(String filename) throws IOException {
		
		S3Object object = s3.getObject(new GetObjectRequest(rootPath, filename));
		InputStream input = object.getObjectContent();
		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int length = (int)object.getObjectMetadata().getContentLength();
		int nRead;
		byte[] data = new byte[length];

		while ((nRead = input.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}

	@Override
	public String getProviderIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

}
