
package udc.master.storage.raid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class RAIDTester 
{
	int blockSize = 11;
	int driveNum = 0;
	VirtualRAID raid;
	
	public RAIDTester()
	{
		raid = new StrippedRAID(3, 1, 10);
		createFileDrives(raid, 4, "/home/gonzalo/CloudTest/HDD");
	}
	
	public void upload(String filename, String path) throws IOException
	{
		byte[] data = new byte[0];
		File file = new File(path);
		
		try 
		{
			FileInputStream fis = new FileInputStream(file);
			data = new byte[(int)file.length()];
			fis.read(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		raid.write(data);
		
	}
	
	public void download(String filename, String path) throws IOException
	{
		/*byte[] data = fs.open(filename);
		
		try {	
			File file = new File(path);
			FileOutputStream fos = new FileOutputStream(file);
			
			file.createNewFile();
			
			fos.write(data);
			fos.flush();
			fos.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	

	
	private void createFileDrives(VirtualRAID raid, int number, String path)
	{
		for(int i = 0; i < number; i++)
		{
			String hddPath = path + "/" + driveNum;
			File directory = new File(hddPath);
			
			if(!directory.exists())
				directory.mkdir();
			
			IVirtualHDD hdd = new FileHDD(hddPath);
			raid.addHDD(hdd);
			driveNum++;
		}
	}
}