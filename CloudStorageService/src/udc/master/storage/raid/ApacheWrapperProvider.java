package udc.master.storage.raid;

import java.io.IOException;
import java.io.InputStream;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;

import com.google.common.io.ByteSource;

public class ApacheWrapperProvider extends CloudProvider {
	String provider;
	
	public ApacheWrapperProvider(String accessKey, String secretKey,
			String rootPath, String provider) {
		super(accessKey, secretKey, rootPath);
		this.provider = provider;
	}

	@Override
	public void uploadFile(String filename, byte[] content) throws IOException {
		
		BlobStore blobStore = ContextBuilder.newBuilder(provider)
		    .credentials(accessKey, secretKey)
		    .buildView(BlobStoreContext.class)
		    .getBlobStore();

		blobStore.createContainerInLocation(null, rootPath);

		ByteSource payload = ByteSource.wrap(content);
		
		Blob blob =  blobStore.blobBuilder(filename)
		    .payload(payload)
		    .contentLength(payload.size())
		    .build();
		blobStore.putBlob(rootPath, blob);
	}

	@Override
	public byte[] downloadFile(String filename) throws IOException {
		BlobStore blobStore = ContextBuilder.newBuilder(provider)
			    .credentials(accessKey, secretKey)
			    .buildView(BlobStoreContext.class)
			    .getBlobStore();

			blobStore.createContainerInLocation(null, rootPath);
			Blob blob =  blobStore.getBlob(rootPath, filename);
			InputStream stream = blob.getPayload().openStream();

			return getBytesFromInputStream(stream);
	}
	
	public static byte[] getBytesFromInputStream(InputStream is)
	{
	    try (ByteArrayOutputStream os = new ByteArrayOutputStream();)
	    {
	        byte[] buffer = new byte[0xFFFF];

	        for (int len; (len = is.read(buffer)) != -1;)
	            os.write(buffer, 0, len);

	        os.flush();

	        return os.toByteArray();
	    }
	    catch (IOException e)
	    {
	        return null;
	    }
	}
	
	String identifier;

	@Override
	public String getProviderIdentifier() {
		// TODO Auto-generated method stub
		return identifier;
	}
	
	public void setIdentifier(String id) {
		 identifier = id;
	}

}
