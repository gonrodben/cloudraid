
package udc.master.storage.raid;

import java.io.IOException;
import java.util.List;


public interface IVirtualHDD {
	
	String getIdentifier();
	void setIdentifier(String id);
	List<BlockInfo> write(byte[] data) throws IOException;
	void read(List<BlockInfo> blocks, byte[] buffer, int startIndex) throws IOException;
	boolean delete(List<BlockInfo> blocks) throws IOException;
	int getDrivesNumber();
}
