
package udc.master.storage.raid;

import java.util.List;


public class RAIDHelper 
{
	public static byte[] getSection(byte[] data, int index, int length)
	{
		byte [] block = new byte[length];
		
		for(int i = 0; i < length; i++)
			block[i] = data[index + i];
		
		return block;
	}

	public static void setSection(byte[] data, byte[] block, int index)
	{
		for(int i = 0; i < block.length; i++)
			data[index + i] = block[i];
	}
	
	public static byte[] concatSections(List<byte[]> sections)
	{
		int length = 0;
		
		for(int i = 0; i < sections.size(); i++)
			length += sections.get(i).length;
		
		byte[] data = new byte[length];
		int currentIndex = 0;
		
		for(int i = 0; i < sections.size(); i++)
		{
			byte[] portion = sections.get(i);
			
			for(int j = 0; j < portion.length; j++)
			{
				data[currentIndex] = portion[j];
				currentIndex++;
			}
		}
		
		return data;
	}

	public static int min(int a, int b)
	{
		if(a < b)
			return a;
		
		return b;
	}
	
	public static int max(int a, int b)
	{
		if(a > b)
			return a;
		
		return b;
	}
	
	public static byte[] XOR(byte[] block1, byte[] block2)
	{
		int length = max(block1.length, block2.length);
		
		byte[] xorBlock = new byte[length];
		
		for(int i = 0; i < length; i++)
		{
			byte b1 = 0;
			byte b2 = 0;
			
			if(i < block1.length)
				b1 = block1[i];
			
			if(i < block2.length)
				b2 = block2[i];
			
			xorBlock[i] = (byte)(b1 ^ b2);
		}
		
		return xorBlock;
	}


}
