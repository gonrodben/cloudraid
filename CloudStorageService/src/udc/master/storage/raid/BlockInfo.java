
package udc.master.storage.raid;

public class BlockInfo 
{
	private String hddId;
	private String blockId;
	public int index;
	public boolean isParityBlock;
	public int row;
	private int size;
	
	
	public BlockInfo(String hddId, String blockId, int size)
	{
		this.hddId = hddId;
		this.blockId = blockId;
		this.size = size;
	}
	
	public String getHDDIdentifier()
	{
		return hddId;
	}
	
	public String getBlockIdentifier()
	{
		return blockId;
	}

	public int getSize()
	{
		return size;
	}
}
