package udc.master.storage.raid;

import java.io.IOException;


public abstract class CloudProvider {
	
	protected String accessKey;
	protected String secretKey;
	protected String rootPath;
	
	public CloudProvider(String accessKey, String secretKey, String rootPath)
	{
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.rootPath = rootPath;
	}
	
	public abstract void uploadFile(String filename, byte [] content) throws IOException;
	
	public abstract byte [] downloadFile(String filename) throws IOException;
	
	public abstract String getProviderIdentifier();
}
