
package udc.master.storage.raid;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class FileHDD implements IVirtualHDD {

	String dataPath;
	
	public FileHDD(String path)
	{
		dataPath = path;
	}
	
	@Override
	public String getIdentifier()
	{
		return dataPath;
	}
	
	@Override
	public void setIdentifier(String id)
	{
		dataPath = id;
	}
	
	@Override
	public List<BlockInfo> write(byte[] data) throws IOException {
		
		String filename = java.util.UUID.randomUUID().toString();
		String blockPath = dataPath + "/" + filename;
			
		File file = new File(blockPath);
		FileOutputStream fos = new FileOutputStream(file);
			
		file.createNewFile();
			
		fos.write(data);
		fos.flush();
		fos.close();
		
		List<BlockInfo> blocks = new ArrayList<BlockInfo>();
		BlockInfo block = new BlockInfo(getIdentifier(), filename, data.length);
		blocks.add(block);
		return blocks;
	}

	@Override
	public void read(List<BlockInfo> blocks, byte[] buffer, int startIndex) throws IOException {
		
		if(blocks.size() != 1)
			return;
		
		byte [] data = read(blocks.get(0));
		RAIDHelper.setSection(buffer, data, startIndex);
	}
	
	private byte[] read(BlockInfo block) throws IOException {

			String blockPath = dataPath + "/" + block.getBlockIdentifier();
			File file = new File(blockPath);
		
			FileInputStream fis = new FileInputStream(file);
			byte [] data = new byte[(int)file.length()];
			fis.read(data);
		
			fis.close();
			return data;
	}

	
	@Override
	public boolean delete(List<BlockInfo> blocks) throws IOException
	{
		boolean result = true;
		
		for(int i = 0; i < blocks.size(); i++)
		{
			BlockInfo block = blocks.get(i);
			String blockPath = dataPath + "/" + block.getBlockIdentifier();
			File file = new File(blockPath);
			if(!file.delete())
				result = false;
		}
		
		return result;
	}
	
	@Override
	public int getDrivesNumber()
	{
		return 1;
	}

}
