package udc.master.storage;

public class DataFactory 
{
	static DataService dataService;
	
	public static DataService getDataService()
	{
		if(dataService == null)
			dataService = new DataService(getDataProvider());
		
		return dataService;
	}
	
	private static IDataProvider getDataProvider()
	{
		return new MySqlDataProvider();
	}
}
