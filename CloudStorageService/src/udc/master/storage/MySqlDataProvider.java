
package udc.master.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import udc.master.storage.log.Log;



public class MySqlDataProvider implements IDataProvider
{
	public int getBucketPrivileges(String bucket, String username)
	{
		int result = -1;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT ub.privileges "
	        	+ "FROM user_buckets ub "
	        	+ "INNER JOIN buckets b ON b.name = ub.bucket "		  
	        	+ "WHERE b.name LIKE ? AND ub.username = ?");

			stmt.setString(1, bucket);
			stmt.setString(2, username);
			rs = stmt.executeQuery();
			      
			 if(rs.next())
				 result = rs.getInt("privileges");
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error getting user privileges to bucket", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
		
		return result;
	}
	
	public List<Bucket> listBuckets(String username)
	{
		List<Bucket> buckets = new ArrayList<Bucket>();
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT b.name, ub.privileges "
	        	+ "FROM user_buckets ub "
	        	+ "INNER JOIN buckets b ON b.name = ub.bucket "		  
	        	+ "WHERE ub.username LIKE ?");

			stmt.setString(1, username);
			rs = stmt.executeQuery();
			      
			 while(rs.next())
			 {
				 Bucket bucket = new Bucket();
				 bucket.name = rs.getString("name");
				 bucket.privileges = rs.getInt("privileges");
				 
				 buckets.add(bucket);
			 }  
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error listing buckets", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}

		return buckets;
	}
	
	public void insertBucket(String name)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "INSERT INTO buckets (name) "
				+ "VALUES (?)");

			stmt.setString(1, name);
			stmt.executeUpdate();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error inserting bucket", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}
	
	public void shareBucket(String bucket, String username, int privileges)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "INSERT INTO user_buckets (bucket, username, privileges) "
				+ "VALUES (?, ?, ?)");

			stmt.setString(1, bucket);
			stmt.setString(2, username);
			stmt.setInt(3, privileges);

			stmt.execute();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error sharing bucket", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}
	
	public void unshareBucket(String bucket, String username)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "DELETE FROM user_buckets "
				+ "WHERE bucket LIKE ? AND username LIKE ?");

			stmt.setString(1, bucket);
			stmt.setString(2, username);

			stmt.execute();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error unsharing bucket", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}

	public Boolean existsBucket(String bucket)
	{
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Boolean exists = false;
		
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "SELECT count(*) FROM buckets "
				+ "WHERE name LIKE ?");

			stmt.setString(1, bucket);
			rs = stmt.executeQuery();
			
			if(rs.next())
			{
				int result = rs.getInt(1);
				exists = result > 0;
			}
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error checking bucket exists", ex);	
			exists = true;
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
		
		return exists;
	}
	
	public void insertUser(User user)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "INSERT INTO users (name, email, username, password) "
				+ "VALUES (?, ?, ?, ?)");

			stmt.setString(1, user.name);
			stmt.setString(2, user.email);
			stmt.setString(3, user.username);
			stmt.setString(4, user.password);
			
			stmt.executeUpdate();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error inserting user", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}
	
	public User getUser(String username)
	{
		User user = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT name, email, username, password "
	        	+ "FROM users "	  
	        	+ "WHERE username LIKE ?");

			stmt.setString(1, username);
			rs = stmt.executeQuery();
			      
			 if(rs.next())
			 {
				 user = new User();
				 user.name = rs.getString("name");
				 user.email = rs.getString("email");
				 user.username = rs.getString("username");
				 user.password = rs.getString("password");
			 }  
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error getting user", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
		
		return user;
	}

	public int insertFile(File file)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "INSERT INTO files (bucket, path, size, isFolder, config) "
				+ "VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

			stmt.setString(1, file.bucket);
			stmt.setString(2, file.path);
			stmt.setInt(3, file.size);
			stmt.setBoolean(4, file.isFolder);
			stmt.setString(5, file.config);
			
			stmt.executeUpdate();
			
			ResultSet keys = stmt.getGeneratedKeys();    
			keys.next();  
			return keys.getInt(1);
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error inserting file", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
		
		return 0;
	}
	
	public void deleteFile(int fileId)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement("DELETE FROM files WHERE fileId = ? ");

			stmt.setInt(1, fileId);
			
			stmt.executeUpdate();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error deleting file", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}
	
	public List<File> listFiles(String bucket)
	{
		List<File> files = new ArrayList<File>();
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT fileId, bucket, path, size, isFolder, config "
	        	+ "FROM files "		  
	        	+ "WHERE bucket LIKE ?");

			stmt.setString(1, bucket);
			rs = stmt.executeQuery();
			      
			 while(rs.next())
			 {
				 File file = new File();
				 file.fileId = rs.getInt("fileId");
				 file.bucket = rs.getString("bucket");
				 file.path = rs.getString("path");
				 file.size = rs.getInt("size");
				 file.isFolder = rs.getBoolean("isFolder");
				 file.config = rs.getString("config");
				 
				 files.add(file);
			 }  
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error listing files", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}

		return files;
	}

	public File getFile(String bucket, String path)
	{
		File file = null;		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT fileId, bucket, path, size, isFolder, config "
	        	+ "FROM files "		  
	        	+ "WHERE bucket LIKE ? AND path LIKE ?");

			stmt.setString(1, bucket);
			stmt.setString(2, path);
			rs = stmt.executeQuery();
			      
			 while(rs.next())
			 {
				 file = new File();
				 file.fileId = rs.getInt("fileId");
				 file.bucket = rs.getString("bucket");
				 file.path = rs.getString("path");
				 file.size = rs.getInt("size");
				 file.isFolder = rs.getBoolean("isFolder");
				 file.config = rs.getString("config");
			 }  
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error getting file", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}

		return file;
	}
	
	public void insertBlock(DataBlock block)
	{
		Connection conn = null;
		PreparedStatement stmt = null;

		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
				  "INSERT INTO blocks (fileId, deviceId, name, size, coding, stripRow, stripIndex ) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?)");

			stmt.setInt(1, block.fileId);
			stmt.setString(2, block.deviceId);
			stmt.setString(3, block.name);
			stmt.setInt(4, block.size);
			stmt.setBoolean(5, block.coding);
			stmt.setInt(6, block.stripRow);
			stmt.setInt(7, block.index);
			
			stmt.executeUpdate();
		} 
		catch(SQLException ex) 
		{
			Log.RecordError(Configuration.getSourceName(), "Error inserting block", ex);	
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
	}
	
	public List<DataBlock> listBlocks(int fileId)
	{
		List<DataBlock> blocks = new ArrayList<DataBlock>();
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
 
		try 
		{
			conn = DriverManager.getConnection(Configuration.getConnectionString());
			stmt = conn.prepareStatement(
	        	  "SELECT blockId, fileId, deviceId, name, size, coding, stripRow, stripIndex "
	        	+ "FROM blocks "		  
	        	+ "WHERE fileId = ?");

			stmt.setInt(1, fileId);
			rs = stmt.executeQuery();
			      
			 while(rs.next())
			 {
				 DataBlock block = new DataBlock();
				 
				 block.blockId = rs.getInt("blockId");
				 block.fileId = rs.getInt("fileId");
				 block.deviceId = rs.getString("deviceId");
				 block.name = rs.getString("name");
				 block.size = rs.getInt("size");
				 block.coding = rs.getBoolean("coding");
				 block.stripRow = rs.getInt("stripRow");
				 block.index = rs.getInt("stripIndex");
				 
				 blocks.add(block);
			 }  
		} 
		catch(SQLException ex) {
			Log.RecordError(Configuration.getSourceName(), "Error listing blocks", ex);
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}

		return blocks;
	}
}
