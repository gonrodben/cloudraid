
package udc.master.storage;

import java.util.List;


public interface IDataProvider {

	void insertBucket(String name);
	void shareBucket(String bucket, String username, int privileges);
	void unshareBucket(String bucket, String username);
	List<Bucket> listBuckets(String username);
	int getBucketPrivileges(String bucket, String username);
	Boolean existsBucket(String bucket);
	
	void insertUser(User user);
	User getUser(String username);
	
	int insertFile(File file);
	void deleteFile(int fileId);
	List<File> listFiles(String bucket);
	File getFile(String bucket, String path);
	
	void insertBlock(DataBlock block);
	List<DataBlock> listBlocks(int fileId);
}
