package udc.master.storage;

public enum BucketAccess {
	NONE, READ, WRITE, ADMIN
}
