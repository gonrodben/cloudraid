
package udc.master.storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import udc.master.storage.log.Log;
import udc.master.storage.raid.BlockInfo;
import udc.master.storage.raid.VirtualRAID;


public class FileSystem 
{
	public FileSystem()
	{
		Configuration.loadConfiguration();
	}
	
	public boolean verifyUser(String username, String password)
	{
		User user = DataFactory.getDataService().getUser(username);
		
		if(user == null || !user.password.equals(password))
			return false;
		
		return true;
	}
	
	public void createUser(String username, String password, String name, String email) throws DataStorageException
	{
		User user = DataFactory.getDataService().getUser(username);
		
		if(user != null)
			throw new DataStorageException("Ya existe el usuario.");
		
		user = new User();
		user.username = username;
		user.password = password;
		user.name = name;
		user.email = email;
		
		DataFactory.getDataService().insertUser(user);
	}
	
	public void shareBucket(String bucket, String username, BucketAccess access) throws DataStorageException
	{
		if(!DataFactory.getDataService().existsBucket(bucket))
			throw new DataStorageException("El bucket no es válido.");
		
		User user = DataFactory.getDataService().getUser(username);
		
		if(user == null)
			throw new DataStorageException("El usuario no es válido.");
		
		DataFactory.getDataService().unshareBucket(bucket, username);
		DataFactory.getDataService().shareBucket(bucket, username, access.ordinal());
	}
	
	public void unshareBucket(String username, String bucket)
	{
		DataFactory.getDataService().unshareBucket(bucket, username);
	}
	
	public List<String> listUserBuckets(String username)
	{
		List<String> names = new ArrayList<String>();
		List<Bucket> buckets = DataFactory.getDataService().listBuckets(username);
		
		for(int i = 0; i < buckets.size(); i++)
			names.add(buckets.get(i).name);
		
		return names;
	}
	
	public void createBucket(String bucket, String username) throws DataStorageException
	{
		if(DataFactory.getDataService().existsBucket(bucket))
			throw new DataStorageException("Ya existe un bucket con el mismo nombre.");
		
		User user = DataFactory.getDataService().getUser(username);
		
		if(user == null)
			throw new DataStorageException("El usuario no es válido.");
		
		DataFactory.getDataService().insertBucket(username, bucket);
	}
	
	public List<String> listBucketObjectsPath(String bucket, String username)
	{
		if(DataFactory.getDataService().getBucketPrivileges(bucket, username) < 1)
			return new ArrayList<String>();
		
		List<String> paths = new ArrayList<String>();
		List<File> files = DataFactory.getDataService().listFiles(bucket);
		
		for(int i = 0; i < files.size(); i++)
			paths.add(files.get(i).path);
		
		return paths;
	}
	
	public void addFile(String bucket, String username, String path, byte [] data) throws DataStorageException
	{
		int privileges = DataFactory.getDataService().getBucketPrivileges(bucket, username);
		
		if(privileges < 2)
			throw new DataStorageException("El bucket seleccionado no es válido.");
		
		File file = DataFactory.getDataService().getFile(bucket, path);
		
		if(file != null)
			throw new DataStorageException("El fichero ya existe. Debe actualizar el contenido.");
		
		List<BlockInfo> blocks = null;
		
		try 
		{
			blocks = getRAID().write(data);
		} 
		catch (IOException e) {
			Log.RecordError(Configuration.getSourceName(), "Error almacenando los datos en el RAID", e);
			throw new DataStorageException("Error almacenando los datos en el RAID");
		}
		
		createFile(bucket, path, blocks, data.length, getRAID().getConfiString());
	}
	
	public void updateFile(String bucket, String username, String path, byte [] data) throws DataStorageException
	{
		int privileges = DataFactory.getDataService().getBucketPrivileges(bucket, username);
		
		if(privileges < 2)
			throw new DataStorageException("El bucket seleccionado no es válido.");
		
		File file = DataFactory.getDataService().getFile(bucket, path);
		
		if(file == null)
			throw new DataStorageException("El fichero no existe. Debe crearlo.");
		
		if(!deleteFile(file))
			throw new DataStorageException("Error al antualizar los datos.");
		
		addFile(bucket, username, path, data);
	}
	
	public boolean deleteFile(String bucket, String username, String path) throws DataStorageException
	{
		int privileges = DataFactory.getDataService().getBucketPrivileges(bucket, username);
		
		if(privileges < 2)
			throw new DataStorageException("El bucket no es válido.");
		
		File file = DataFactory.getDataService().getFile(bucket, path);
		
		if(file == null)
			throw new DataStorageException("El fichero no existe.");
		
		return deleteFile(file);
	}
	
	public boolean deleteFile(File file)
	{
		List<BlockInfo> blocks = getBlockInfoList(file.fileId);
		 
		//Se eliminan los bloques anteriores
		try {
			if(!getRAID().delete(blocks))
				return false;
		} catch (IOException e1) {
			Log.RecordError(Configuration.getSourceName(), "Error mientras se actualizaban los datos.", e1);
			return false;
		}
		
		DataFactory.getDataService().deleteFile(file.fileId);
		return true;
	}

	public byte [] getFileData(String bucket, String username, String path) throws DataStorageException
	{
		int privileges = DataFactory.getDataService().getBucketPrivileges(bucket, username);
		
		if(privileges < 1)
			throw new DataStorageException("El bucket seleccionado no es válido.");
		
		File file = DataFactory.getDataService().getFile(bucket, path);
		
		if(file == null)
			throw new DataStorageException("El fichero no es válido.");
		
		List<BlockInfo> blocks = getBlockInfoList(file.fileId);
		byte [] data = new byte[file.size];
		try 
		{
			getRAID().read(blocks, data, 0);
		} 
		catch (IOException e) {
			Log.RecordError(Configuration.getSourceName(), "Error obteniendo los datos en el RAID", e);
			throw new DataStorageException("Error obteniendo los datos en el RAID");
		}
		
		return data;
	}
	
	public void createFolder(String bucket, String path)
	{
		File file = new File();
		
		file.bucket = bucket;
		file.path = path;
		file.size = 0;
		file.isFolder = true;
		file.config = "";
		
		DataFactory.getDataService().insertFile(file);
	}

	private List<BlockInfo> getBlockInfoList(int fileId)
	{
		List<BlockInfo> blocks = new ArrayList<BlockInfo>();
		
		List<DataBlock> dataBlocks = DataFactory.getDataService().listBlocks(fileId);
 		
		for(int i = 0; i < dataBlocks.size(); i++)
		{
			DataBlock dataBlock = dataBlocks.get(i);
			BlockInfo block = new BlockInfo(dataBlock.deviceId, dataBlock.name, dataBlock.size);
			block.index = dataBlock.index;
			block.row = dataBlock.stripRow;
			block.isParityBlock = dataBlock.coding;
			
			blocks.add(block);
		}
		
		return blocks;
	}
	
	private void createFile(String bucket, String path, List<BlockInfo> blocks, int size, String config) throws DataStorageException
	{
		File file = new File();
		
		file.bucket = bucket;
		file.path = path;
		file.size = size;
		file.isFolder = false;
		file.config = config;
		
		int fileId = DataFactory.getDataService().insertFile(file);
		
		if(fileId == 0)
			throw new DataStorageException("Error guardando los metadatos del fichero");
		
		for(int i = 0; i < blocks.size(); i++)
		{
			BlockInfo block = blocks.get(i);
			DataBlock dataBlock = new DataBlock();
			
			dataBlock.fileId = fileId;
			dataBlock.deviceId = block.getHDDIdentifier();
			dataBlock.name = block.getBlockIdentifier();
			dataBlock.index = block.index;
			dataBlock.stripRow = block.row;
			dataBlock.coding = block.isParityBlock;
			dataBlock.size = block.getSize();
			
			DataFactory.getDataService().insertBlock(dataBlock);
		}
	}

	private VirtualRAID getRAID()
	{
		return Configuration.getRAID();
	}
	
}
