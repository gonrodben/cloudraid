package udc.master.storage;

public class DataBlock {
	public int blockId;
	public int fileId;
	public String deviceId;
	public String name;
	public int size;
	public int stripRow;
	public int index;
	public Boolean coding;
}
