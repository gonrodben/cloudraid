package udc.master.storage.cloud;

import java.io.IOException;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;

import com.google.common.io.ByteSource;

public class ApacheWrapperProvider extends CloudProvider {
	String provider;
	
	public ApacheWrapperProvider(String accessKey, String secretKey,
			String rootPath, String provider) {
		super(accessKey, secretKey, rootPath);
		this.provider = provider;
	}

	@Override
	public void uploadFile(String filename, byte[] content) throws IOException {
		
		BlobStore blobStore = ContextBuilder.newBuilder(provider)
		    .credentials(accessKey, secretKey)
		    .buildView(BlobStoreContext.class)
		    .getBlobStore();

		blobStore.createContainerInLocation(null, rootPath);

		ByteSource payload = ByteSource.wrap(content);
		
		Blob blob =  blobStore.blobBuilder(filename)
		    .payload(payload)
		    .contentLength(payload.size())
		    .build();
		blobStore.putBlob(rootPath, blob);
	}

	@Override
	public byte[] downloadFile(String filename) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProviderIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

}
