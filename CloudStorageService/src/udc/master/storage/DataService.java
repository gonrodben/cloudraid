
package udc.master.storage;

import java.util.List;

public class DataService 
{
	IDataProvider dataProvider;
	
	public DataService(IDataProvider dataProvider)
	{
		this.dataProvider = dataProvider;
	}

	public void insertBucket(String username, String name)
	{
		dataProvider.insertBucket(name);
		shareBucket(name, username, 2);
	}
	
	public void shareBucket(String bucket, String username, int privileges)
	{
		dataProvider.shareBucket(bucket, username, privileges);
	}
	
	public void unshareBucket(String bucket, String username)
	{
		dataProvider.unshareBucket(bucket, username);
	}

	public List<Bucket> listBuckets(String username)
	{
		return dataProvider.listBuckets(username);
	}
	
	public int getBucketPrivileges(String bucket, String username)
	{
		return dataProvider.getBucketPrivileges(bucket, username);
	}
	
	public Boolean existsBucket(String bucket)
	{
		return dataProvider.existsBucket(bucket);
	}
	
	public void insertUser(User user)
	{
		dataProvider.insertUser(user);
	}
	
	public User getUser(String username)
	{
		return dataProvider.getUser(username);
	}

	public int insertFile(File file)
	{
		return dataProvider.insertFile(file);
	}
	
	public void deleteFile(int fileId)
	{
		dataProvider.deleteFile(fileId);
	}
	
	public List<File> listFiles(String bucket)
	{
		return dataProvider.listFiles(bucket);
	}
	
	public File getFile(String bucket, String path)
	{
		return dataProvider.getFile(bucket, path);
	}

	public void insertBlock(DataBlock block)
	{
		dataProvider.insertBlock(block);
	}
	
	public List<DataBlock> listBlocks(int fileId)
	{
		return dataProvider.listBlocks(fileId);
	}
}
