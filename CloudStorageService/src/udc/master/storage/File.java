
package udc.master.storage;

public class File 
{
	public int fileId;
	public String bucket;
	public String path;
	public int size;
	public Boolean isFolder;
	public String config;
}
