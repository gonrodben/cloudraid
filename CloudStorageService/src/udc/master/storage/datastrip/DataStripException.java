
package udc.master.storage.datastrip;

public class DataStripException extends Exception {
	public DataStripException(String message)
	{
		super(message);
	}
}
