
package udc.master.storage.datastrip;

import java.util.List;

public abstract class DataStripper {
	
	private int k;
	private int m;
	private int blockSize;
	
	public int getM()
	{
		return m;
	}
	
	public int getK()
	{
		return k;
	}
	
	public int getBlockSize()
	{
		return blockSize;
	}
	
	public DataStripper(int k, int m, int blockSize)
	{
		this.k = k;
		this.m = m;
		this.blockSize = blockSize;
	}
	
	public abstract List<Block> encode(byte[] data);
	public abstract byte[] decode(List<Block> blocks);
	
	public abstract List<Block> encode(byte[] data, int startIndex, int length);
	public abstract void decode(List<Block> blocks, byte[] data, int startIndex) throws DataStripException;
	
	
}