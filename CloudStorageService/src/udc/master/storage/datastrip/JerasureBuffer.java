
package udc.master.storage.datastrip;

import java.util.Iterator;


public class JerasureBuffer  implements Iterator<byte[]>{

	byte[] data;
	int blockSize;
	int currentIndex;
	int start;
	int end;
	
	public JerasureBuffer(byte[] data, int blockSize) 
	{
		this(data, 0, data.length, blockSize);
	}
	
	public JerasureBuffer(byte[] data, int start, int end, int blockSize)
	{
		this.data = data; 
		this.blockSize = blockSize;
		currentIndex = 0; 
		this.start = start;
		this.end = end;
	}

	@Override
	public boolean hasNext() {
		return currentIndex < end;
	}

	@Override
	public byte[] next() {
		
		byte[] block = new byte[blockSize];
		
		for(int i = 0; i < blockSize; i++)
		{
			if(currentIndex < data.length)
			{
				block[i] = data[currentIndex];
				currentIndex++;
			}
			else
				block[i] = 0;
		}
		
		return block;
	}

	@Override
	public void remove() {
	}

	

}
