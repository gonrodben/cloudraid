
package udc.master.storage.datastrip;

public class Block {
	private byte [] data;
	private Boolean coding;
	public int index;
	public int row;
	
	
	public Block(byte [] data, Boolean coding, int index, int row)
	{
		this.data = data;
		this.coding = coding;
		this.index = index;
		this.row = row;
	}
	
	public byte [] getData()
	{
		return data;
	}
	
	public void setData(byte [] data)
	{
		this.data = data;
	}
	
	public Boolean IsCoding()
	{
		return coding;
	}

	public int getIndex()
	{
		return index;
	}

	public int getRow()
	{
		return row;
	}
}