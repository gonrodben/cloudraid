
package udc.master.storage.datastrip;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.uni_postdam.hpi.jerasure.Decoder;
import de.uni_postdam.hpi.jerasure.Encoder;


public class JerasureDataStripper extends DataStripper {

	public JerasureDataStripper(int k, int m, int blockSize) {
		super(k, m, blockSize);
	}

	@Override
	public List<Block> encode(byte[] data) {
		
		JerasureBuffer buffer = new JerasureBuffer(data, getK()*getBlockSize());
		return encode(buffer);
	}
	
	@Override
	public List<Block> encode(byte[] data, int startIndex, int length) {
		JerasureBuffer buffer = new JerasureBuffer(data, startIndex, length, getK()*getBlockSize());
		return encode(buffer);
	}
	
	private List<Block> encode(JerasureBuffer buffer)
	{
		List<Block> list = new ArrayList<Block>();
		int row = 0;
		int w = getBlockSize();
		Encoder e = new Encoder(getK(), getM(), 8);
		
		while(buffer.hasNext())
		{
			byte [] data = buffer.next();
			byte [] encoding = e.encode(data, getBlockSize()/8);
			
			addBlocks(list, new JerasureBuffer(data, getBlockSize()), false, row);
			addBlocks(list, new JerasureBuffer(encoding, getBlockSize()), true, row);
			row++;
		}
		
		return list;
	}
	
	private void addBlocks(List<Block> list, JerasureBuffer buffer, Boolean coding, int row)
	{
		int index = 0;
		while(buffer.hasNext())
		{
			byte [] data = buffer.next();
			Block block = new Block(data, coding, index, row);
			list.add(block);
			index++;
		}
	}
	

	@Override
	public void decode(List<Block> blocks, byte[] data, int startIndex) throws DataStripException
	{
		int dataFails = 0;
		int codingFails = 0;
		Block [] blockArray = new Block [getK() + getM()];
		
		for(int i = 0; i < blocks.size(); i++)
		{
			Block block = blocks.get(i);
			
			if(block.getData() == null)
			{
				if(block.IsCoding())
					codingFails++;
				else
					dataFails++;
			}
			
			if(block.IsCoding())
				blockArray[getK() + block.index] = block;
			else
				blockArray[block.index] = block;
		}
		
		int fails = dataFails + codingFails;
		if(fails > getM())
			throw new DataStripException("Los datos no pudieron ser recuperados. Ocurrieron " + fails + " fallos de " + getM() + " permitidos.");
		
		if(dataFails > 0)
			recoverFromFailures(blockArray);
		
		saveData(blockArray, data, startIndex);
	}

	private void recoverFromFailures(Block [] blockArray) throws DataStripException
	{
		byte [] matrix = new byte[(getK() + getM())*getBlockSize()];
		File mainFile = FileUtils.createJerasureMainFile();
		Decoder dec = new Decoder(mainFile, getK(), getM(), 8);
		
		List<Block> missingBlocks = new ArrayList<Block>();
		for(int i = 0; i < blockArray.length; i++)
		{
			Block block = blockArray[i];
			if(block.getData() == null)
			{
				if(!block.IsCoding())
					missingBlocks.add(block);						
			}
			else
			{
				FileUtils.createJerasureFile(mainFile, block.IsCoding(), block.index + 1);
				
				for(int j = 0; j < getBlockSize(); j++)
					matrix[i*getBlockSize() + j] = block.getData()[j];
			}
		}
		
		byte [] decoded = dec.decode(matrix, getBlockSize()/8);
		
		for(int i = 0; i < missingBlocks.size(); i++)
		{
			byte[] blockData = new byte[getBlockSize()];
			
			for(int j = 0; j < getBlockSize(); j++)
				blockData[j] = decoded[i*getBlockSize() + j];
			
			missingBlocks.get(i).setData(blockData);
		}
	}
	
	private void saveData(Block [] blockArray, byte[] data, int startIndex) 
	{
		for(int i = 0; i < getK(); i++)
		{
			Block block = blockArray[i];
			int j = 0;
			
			while((startIndex + (i*getBlockSize()) + j) < data.length && j < getBlockSize())
			{
				data[startIndex + (i*getBlockSize()) + j] = block.getData()[j];
				j++;
			}
		}
	}


	@Override
	public byte[] decode(List<Block> blocks) {
		// TODO Auto-generated method stub
		return null;
	}}
