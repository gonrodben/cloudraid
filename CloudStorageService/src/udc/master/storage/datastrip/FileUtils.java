
package udc.master.storage.datastrip;

import java.io.File;
import java.io.IOException;

import udc.master.storage.log.Log;


public class FileUtils 
{
	public static File createJerasureMainFile() throws DataStripException
	{
		File dir = clearJerasureDirectory();
		String filename = dir.getPath() + File.separator + "jerasure";
		File file = new File(filename);
		
		if (!file.exists()) {

				try {
					file.createNewFile();
				} 
				catch (IOException e) {
					Log.RecordError("DataStripService", "Error creando el fichero Jerasures", e);
			        throw new DataStripException("Error creando el fichero Jerasures");
				}
		}
		
		return file;
	}
	
	public static File getJerasureDirectory() throws DataStripException
	{
		String folder = "Erasures";
		File dir = new File(folder);

		if (!dir.exists()) {
			try
			{
			   dir.mkdir();
			} 
			catch(SecurityException se){
			        Log.RecordError("RAIDService", "Error creando carpeta Erasures ", se);
			        throw new DataStripException("No hay permisos para crear la carpeta de Jerasure");
			}
		}
		
		return dir;
	}
	
	public static File clearJerasureDirectory() throws DataStripException
	{
		File dir = getJerasureDirectory();
		for(File file: dir.listFiles()) file.delete();
		return dir;
	}
	
	public static void createJerasureFile(File mainFile, boolean coding, int index) throws DataStripException
	{
		String path = mainFile.getParentFile().getAbsolutePath();
		String format =  "%s_%s%02d";
		String name = String.format(format, mainFile.getName(), (coding ? "m" : "k"), index);
	
		
		String filePath = path + File.separator + name;
				
		File file = new File(filePath);
		try {
			file.createNewFile();
		} catch (IOException e) {
			 Log.RecordError("RAIDService", "Error creando ficheros Jerasure", e);
		     throw new DataStripException("Error creando ficheros Jerasure");
		}
	}
}
