package udc.master.storage;

import java.io.File;

import udc.master.storage.raid.AmazonProvider;
import udc.master.storage.raid.ApacheWrapperProvider;
import udc.master.storage.raid.CloudHDD;
import udc.master.storage.raid.FileHDD;
import udc.master.storage.raid.IVirtualHDD;
import udc.master.storage.raid.StrippedRAID;
import udc.master.storage.raid.VirtualRAID;

public class Configuration 
{
	static String connectionString;
	static VirtualRAID raid;
	
	public static String getConnectionString()
	{
		return connectionString;
	}
	
	public static String getSourceName()
	{
		return "StorageService";
	}
	
	public static void loadConfiguration()
	{
		connectionString = "jdbc:mysql://localhost/cloudraid?user=root&password=root";
	}
	
	public static VirtualRAID getRAID()
	{
		if(raid == null)
			createRAID();
		
		return raid;
	}
	
	private static void createRAID()
	{
		raid = new StrippedRAID(2, 1, 1800);
		createFileDrives(10, "/home/gonzalo/CloudTest/HDD");
	}
	
	private static void createFileDrives(int number, String path)
	{
		
		//AmazonProvider provider1 = new AmazonProvider("AKIAJUZZ7DOOEJD2U2OQ", "gjbmB94gpZvb03ydg985hgW2K+P3LshzrekWLVbN", "sdfbucket1123asd");
		ApacheWrapperProvider provider1 = new ApacheWrapperProvider("gonrodben", "G80NYqP3AseDuQv5Qm8BVb9N1QIPyG52sCBTElq7YfYR5/RWLy11DLLaeLH44Zaq9i6pPpoqaXfz5ToVlfnnGg==", "datosseguros", "azureblob");
		CloudHDD hdd1 = new CloudHDD(provider1);
		provider1.setIdentifier("Azure");
		raid.addHDD(hdd1);
		
		ApacheWrapperProvider provider2 = new ApacheWrapperProvider("AKIAJUZZ7DOOEJD2U2OQ", "gjbmB94gpZvb03ydg985hgW2K+P3LshzrekWLVbN", "datosseguros", "aws-s3");
		CloudHDD hdd2 = new CloudHDD(provider2);
		provider2.setIdentifier("Amazon");
		raid.addHDD(hdd2);
		
		
		ApacheWrapperProvider provider3 = new ApacheWrapperProvider("11514204403976-Project:gonrodben", "Gonzo.123", "datosseguros", "hpcloud-objectstorage");
		CloudHDD hdd3 = new CloudHDD(provider3);
		provider3.setIdentifier("HP");
		raid.addHDD(hdd3);
		
		/*int driveNum = 1;
		
		for(int i = 0; i < number; i++)
		{
			String hddPath = path + "/" + driveNum;
			File directory = new File(hddPath);
			
			if(!directory.exists())
				directory.mkdir();
			
			IVirtualHDD hdd = new FileHDD(hddPath);
			raid.addHDD(hdd);
			driveNum++;
		}*/
	}
}
