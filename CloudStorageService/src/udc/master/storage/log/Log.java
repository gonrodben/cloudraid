
package udc.master.storage.log;

public class Log 
{
	public static void RecordError(String source, String message, String exception)
	{
		System.out.print(source);
		System.out.print(message);
		System.out.print(exception);
	}
	
	public static void RecordError(String source, String message, Exception ex)
	{
		System.out.print(source);
		System.out.print(message);
		ex.printStackTrace();
	}
}
